var express = require('express');
var http = require('http');
var https = require('https');
var fs = require('fs');

var app = express();

var port = process.env.PORT || process.env.NODE_HTTP_LISTENER_SERVICE_PORT || 51900,
    ip   = process.env.IP   || process.env.NODE_HTTP_LISTENER_SERVICE_HOST || '0.0.0.0';

//console.log('env: ' + JSON.stringify(process.env));

var privateKey  = fs.readFileSync('certs/key.pem', 'utf8');
var certificate = fs.readFileSync('certs/cert.pem', 'utf8');

var credentials = {key: privateKey, cert: certificate};


app.get('/ns', (req, res) => {
    console.log('request received');

    var cache = [];
    var reqStr = JSON.stringify(req, function(key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                return 'circular reference to ' + key;
            }
            // Store value in our collection
            cache.push(value);
        }
        return value;
    });
    cache = null; // Enable garbage collection
    //console.log(reqStr);

    var callerIp = {
            'x-forwarded-for': req.headers['x-forwarded-for'],
            'connection_remoteAddress': req.connection ? req.connection.remoteAddress : null,
            'socket_remoteAddress': req.socket ? req.socket.remoteAddress : null,
            'connection_socket_remoteAddress': (req.connection && req.connection.socket) ? req.connection.socket.remoteAddres : null
    };

    console.log((new Date()) + ' - call from: ' + JSON.stringify(callerIp));

    res.send('{"status":"ok"}');
});

//app.listen(port, ip);

http.createServer(app).listen(port);
//https.createServer(credentials, app).listen(port);

console.log('running on ' + [ip, port].join(':'));

module.exports = app ;